//
//  ViewController.swift
//  FirstTest
//
//  Created by Ilya on 10/17/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var label: UILabel!
    @IBAction
    func touchButton(_ sender: UIButton) {
        let randomNum = arc4random_uniform(2)
        switch randomNum {
        case 0:
            label.text = "Eagle"
        case 1:
            label.text = "Tails"
        default:
            break;
        }
    }
}

